package utfpr.persistence.controller;

import inscricao.persistence.entity.Candidato;
import inscricao.persistence.entity.Idioma;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

/**
 * Desenvolvimento de aplicações Web
 *
 * @author Wilson Horstmeyer Bogado <wilson@utfpr.edu.br>
 */
public class CandidatoJpaController extends JpaController {

    public CandidatoJpaController() {
    }

    public List<Candidato> findbyIdioma() {
        EntityManager em = null;
        try {
            em = getEntityManager();
            TypedQuery<Candidato> q = em.createQuery(
                    "SELECT c FROM Candidato c ORDER BY c.cpf",
                    Candidato.class);
            return q.getResultList();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Candidato> findbyIdioma(Integer codigo) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            TypedQuery<Candidato> q = em.createQuery(
                    "SELECT c FROM Candidato c WHERE codigo = " + codigo + " ORDER BY c.cpf",
                    Candidato.class);
            return q.getResultList();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Candidato> findbyIdioma(Idioma idioma) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            TypedQuery<Candidato> q = em.createQuery(
                    "SELECT c FROM Candidato c  WHERE codigo = " + idioma.getCodigo() + " ORDER BY c.cpf",
                    Candidato.class);
            return q.getResultList();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }
}
